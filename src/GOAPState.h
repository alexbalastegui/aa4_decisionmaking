#pragma once
class Agent;
#include <time.h>
#include <vector>
#include <iostream>
#include "Vector2D.h"

enum class customBool{_false, _true, _irrelevant};

bool equal(customBool a, customBool b);

class GOAPState{
protected:

	customBool agentAlive;
	customBool agentHasWeapon;
	customBool weaponIsLoaded;
	customBool agentHasBombs;
	customBool enemyIsVisible;
	customBool enemyIsAligned;
	customBool enemyIsNear;
	customBool enemyIsAlive;


	bool taskIsDone=false;

public:
/*
	customBool target_agentAlive;
	customBool target_agentHasWeapon;
	customBool target_weaponIsLoaded;
	customBool target_agentHasBombs;
	customBool target_enemyIsVisible;
	customBool target_enemyIsAligned;
	customBool target_enemyIsNear;
	customBool target_enemyIsAlive;*/

	std::string name;

	

	void printName();
	

	virtual void enter(typename Agent* self, typename Agent* enemy) =0;
	virtual void update(typename Agent* self, typename Agent* enemy, float dtime) =0;
	virtual void exit(typename Agent* self, typename Agent* enemy) =0;
	virtual void resetState() = 0;

	virtual void effect(customBool &agentAlive,
		customBool &agentHasWeapon,
		customBool &weaponIsLoaded,
		customBool &agentHasBombs,
		customBool &enemyIsVisible,
		customBool &enemyIsAligned,
		customBool &enemyIsNear,
		customBool &enemyIsAlive) {};

	virtual void precondition(customBool &agentAlive,
		customBool &agentHasWeapon,
		customBool &weaponIsLoaded,
		customBool &agentHasBombs,
		customBool &enemyIsVisible,
		customBool &enemyIsAligned,
		customBool &enemyIsNear,
		customBool &enemyIsAlive) {};

	virtual void effectHypotesis(bool &agentAlive,
		bool &agentHasWeapon,
		bool &weaponIsLoaded,
		bool &agentHasBombs,
		bool &enemyIsVisible,
		bool &enemyIsAligned,
		bool &enemyIsNear,
		bool &enemyIsAlive) {};


	void addConexionIn(GOAPState* other);
	void addConexionOut(GOAPState* other);

	bool TaskDone();
	
	std::vector<GOAPState*> in;
	std::vector<GOAPState*> out;

	int actionCost;
	int acumulatedValue;
};


