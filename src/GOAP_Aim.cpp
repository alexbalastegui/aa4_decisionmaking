#include "GOAP_Aim.h"

void GOAP_Aim::enter(Agent * self, Agent * enemy)
{
	//lookAtVec = Vector2D::Normalize(Vector2D::Distance(self->getPosition(), enemy->getPosition()));
	actionCost = 6;
	name = "Aim";
	taskIsDone = false;
	
	Vector2D selfEnemy = (self->getPosition() - enemy->getPosition()).Normalize();
	self->setOrientation({selfEnemy.y, selfEnemy.x});
}

void GOAP_Aim::update(Agent * self, Agent * enemy, float dtime)
{

	self->getOrientation();
	lookAtVec = Vector2D::Normalize(enemy->getPosition() - self->getPosition());

	if ((Vector2DUtils::AngleBetweenVecs(self->getOrientation(), lookAtVec) * RAD2DEG) < acceptableRad) {
		//enemyIsAligned = true;
		taskIsDone = true;
	}
	
	self->setOrientation( Vector2D::Lerp(self->getOrientation(), lookAtVec, 0.03f ));

}

void GOAP_Aim::exit(Agent * self, Agent * enemy)
{
}

void GOAP_Aim::resetState()
{
	taskIsDone = false;
}

void GOAP_Aim::effect(customBool & agentAlive, customBool & agentHasWeapon, customBool & weaponIsLoaded, customBool & agentHasBombs, customBool & enemyIsVisible, 
	customBool & enemyIsAligned, customBool & enemyIsNear, customBool & enemyIsAlive)
{
	agentAlive = customBool::_true;
	agentHasWeapon = customBool::_true;
	weaponIsLoaded = customBool::_true;
	agentHasBombs = agentHasBombs;
	enemyIsVisible = customBool::_true;
	enemyIsAligned = customBool::_true;
	enemyIsNear = customBool::_true;
	enemyIsAlive = customBool::_true;
}

void GOAP_Aim::precondition(customBool & agentAlive, customBool & agentHasWeapon, customBool & weaponIsLoaded, customBool & agentHasBombs, customBool & enemyIsVisible,
	customBool & enemyIsAligned, customBool & enemyIsNear, customBool & enemyIsAlive)
{
	agentAlive = customBool::_true;
	agentHasWeapon = customBool::_true;
	weaponIsLoaded = customBool::_true;
	agentHasBombs = customBool::_irrelevant;
	enemyIsVisible = customBool::_true;
	enemyIsAligned = customBool::_irrelevant;
	enemyIsNear = customBool::_true;
	enemyIsAlive = customBool::_true;
}

void GOAP_Aim::effectHypotesis(bool & agentAlive, bool & agentHasWeapon, bool & weaponIsLoaded, bool & agentHasBombs, bool & enemyIsVisible, bool & enemyIsAligned, bool & enemyIsNear, bool & enemyIsAlive)
{
	enemyIsAligned=true;
}




