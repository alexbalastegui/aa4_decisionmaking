#pragma once
#include <vector>
#include <map>
#include <queue>
#include "Agent.h"
#include "Vector2D.h"
#include "Path.h"
#include "GOAPState.h"


class Agent;

class SteeringBehavior
{
public:
	SteeringBehavior();
	~SteeringBehavior();

	Vector2D Seek(Agent *agent, Vector2D target, float dtime);
	Vector2D Seek(Agent *agent, Agent *target, float dtime);
	Vector2D Flee(Agent *agent, Vector2D target, float dtime);
	Vector2D Flee(Agent *agent, Agent *target, float dtime);
	Vector2D Arrive(Agent *agent, Vector2D target, int slow_radius, float dtime);
	Vector2D Arrive(Agent *agent, Agent *target, int slow_radius, float dtime);
	Vector2D SteeringBehavior::OwnWander(Agent * agent, float wanderOffset, float wanderRadius, float wanderMaxChange, float wanderMaxChangeOffset, float dtime);
	Vector2D SimplePathFollowing(Agent *agent, float dtime);
};