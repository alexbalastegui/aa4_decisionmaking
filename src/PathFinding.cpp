#include "PathFinding.h"

#define HEURISTIC_K 1
#define DIJKSTRA_K 1

std::vector<GOAPState*> PathFinding::AStar(GOAPState* source, GOAPState* target)
{
	int check = 0;

	std::priority_queue<frontierVar> frontier;
	std::map<GOAPState*, GOAPState*> parentMap; //clave es nodo, valor es padre
	std::map<GOAPState*, int>costMap;

	customBool agentAlive=customBool::_true;
	customBool agentHasWeapon= customBool::_true;
	customBool weaponIsLoaded= customBool::_false;
	customBool agentHasBombs=customBool::_true;
	customBool enemyIsVisible= customBool::_false;
	customBool enemyIsAligned= customBool::_false;
	customBool enemyIsNear= customBool::_false;
	customBool enemyIsAlive= customBool::_true;

	source->effect(agentAlive, agentHasWeapon, weaponIsLoaded, agentHasBombs, enemyIsVisible, enemyIsAligned, enemyIsNear, enemyIsAlive);


	customBool target_agentAlive;
	customBool target_agentHasWeapon;
	customBool target_weaponIsLoaded;
	customBool target_agentHasBombs;
	customBool target_enemyIsVisible;
	customBool target_enemyIsAligned;
	customBool target_enemyIsNear;
	customBool target_enemyIsAlive;

	target->precondition(target_agentAlive, target_agentHasWeapon, target_weaponIsLoaded, target_agentHasBombs, target_enemyIsVisible, target_enemyIsAligned, target_enemyIsNear, target_enemyIsAlive);

	customBool aux_agentAlive = agentAlive;
	customBool aux_agentHasWeapon = agentHasWeapon;
	customBool aux_weaponIsLoaded = weaponIsLoaded;
	customBool aux_agentHasBombs = agentHasBombs;
	customBool aux_enemyIsVisible = enemyIsVisible;
	customBool aux_enemyIsAligned = enemyIsAligned;
	customBool aux_enemyIsNear = enemyIsNear;
	customBool aux_enemyIsAlive = enemyIsAlive;
	
	//source->effect(agentAlive, agentHasWeapon, weaponIsLoaded, agentHasBombs, enemyIsVisible, enemyIsAligned, enemyIsNear, enemyIsAlive);

	frontier.push({ source, 0 }); //root has no cost
	parentMap[frontier.top().node] = nullptr; //root has no parent
	costMap[frontier.top().node] = 0; //root has no cost

	GOAPState* destinationNode = nullptr;
	bool firstIter = true;

	while (!frontier.empty()) {
		bool canBeExpanded = false;

		frontier.top().node->effect(agentAlive, agentHasWeapon, weaponIsLoaded, agentHasBombs, enemyIsVisible, enemyIsAligned, enemyIsNear, enemyIsAlive);

		frontierVar top = frontier.top();
		frontier.pop();

		for (GOAPState* e : top.node->out) {

			std::map<GOAPState*, GOAPState*>::iterator it = parentMap.find(e);

			if (it == parentMap.end()) { //si el nodo que estamos mirando no ha sido ya visitado
				
				e->precondition(aux_agentAlive, aux_agentHasWeapon, aux_weaponIsLoaded, aux_agentHasBombs, aux_enemyIsVisible, aux_enemyIsAligned, aux_enemyIsNear, aux_enemyIsAlive);

				if (equal(agentAlive, aux_agentAlive)) {
					if (equal(agentHasWeapon, aux_agentHasWeapon)) {
						if (equal(weaponIsLoaded, aux_weaponIsLoaded)) {
							if (equal(agentHasBombs, aux_agentHasBombs)) {
								if (equal(enemyIsVisible, aux_enemyIsVisible)) {
									if (equal(enemyIsAligned, aux_enemyIsAligned)) {
										if (equal(enemyIsNear, aux_enemyIsNear)) {
											if (equal(enemyIsAlive, aux_enemyIsAlive)) {
												
												int heuristicVal = 0;
												if (agentAlive == target_agentAlive || target_agentAlive == customBool::_irrelevant)
													heuristicVal++;
												else if ((agentAlive == customBool::_true && target_agentAlive == customBool::_false) || (agentAlive == customBool::_false && target_agentAlive == customBool::_true))
													heuristicVal--;
												if (agentHasWeapon == target_agentHasWeapon && agentHasWeapon != customBool::_irrelevant)
													heuristicVal++;
												else if ((agentHasWeapon == customBool::_true && target_agentHasWeapon == customBool::_false) || (agentHasWeapon == customBool::_false && target_agentHasWeapon == customBool::_true))
													heuristicVal--;
												if (weaponIsLoaded == target_weaponIsLoaded && weaponIsLoaded != customBool::_irrelevant)
													heuristicVal++;
												else if ((weaponIsLoaded == customBool::_true && target_weaponIsLoaded == customBool::_false) || (weaponIsLoaded == customBool::_false && target_weaponIsLoaded == customBool::_true))
													heuristicVal--;
												if (agentHasBombs == target_agentHasBombs && agentHasBombs != customBool::_irrelevant)
													heuristicVal++;
												else if ((agentHasBombs == customBool::_true && target_agentHasBombs == customBool::_false) || (agentHasBombs == customBool::_false && target_agentHasBombs == customBool::_true))
													heuristicVal--;
												if (enemyIsVisible == target_enemyIsVisible && enemyIsVisible != customBool::_irrelevant)
													heuristicVal++;
												else if ((enemyIsVisible == customBool::_true && target_enemyIsVisible == customBool::_false) || (enemyIsVisible == customBool::_false && target_enemyIsVisible == customBool::_true))
													heuristicVal--;
												if (enemyIsAligned == target_enemyIsAligned && enemyIsAligned != customBool::_irrelevant)
													heuristicVal++;
												else if ((enemyIsAligned == customBool::_true && target_enemyIsAligned == customBool::_false) || (enemyIsAligned == customBool::_false && target_enemyIsAligned == customBool::_true))
													heuristicVal--;
												if (enemyIsNear == target_enemyIsNear && enemyIsNear != customBool::_irrelevant)
													heuristicVal++;
												else if ((enemyIsNear == customBool::_true && target_enemyIsNear == customBool::_false) || (enemyIsNear == customBool::_false && target_enemyIsNear == customBool::_true))
													heuristicVal--;
												if (enemyIsAlive == target_enemyIsAlive && enemyIsAlive != customBool::_irrelevant)
													heuristicVal++;
												else if ((enemyIsAlive == customBool::_true && target_enemyIsAlive == customBool::_false) || (enemyIsAlive == customBool::_false && target_enemyIsAlive == customBool::_true))
													heuristicVal--;

												int acumulatedCost = top.val + e->actionCost;

												float formulaVal = heuristicVal * HEURISTIC_K + acumulatedCost * DIJKSTRA_K;

												costMap[e] = formulaVal;
												frontier.push({ e, acumulatedCost });
												parentMap[e] = top.node;
												canBeExpanded = true;


												if (e == target) {
													//hemos llegado al destino						
													destinationNode = e;
													goto makeAnsVec;
												}

											}
										}
									}
								}
							}
						}
					}
				}				
			}
		}
	}


makeAnsVec:

	std::vector<GOAPState*> ans{};
	if (destinationNode != NULL) {
		GOAPState* addition;

		check = 0;

		while (destinationNode != source) {
			check++;
			ans.push_back(destinationNode);
			destinationNode = parentMap[destinationNode];
		}
		ans.push_back(source);
		std::cout << "The route defined by A* is:" << std::endl;
		for (int i = ans.size()-1; i >= 0; --i) {
			//ans.at(i)->enter({}, {});
			std::cout << "\t" << typeid(*ans.at(i)).name() << std::endl;
		}
		std::cout << std::endl;
	}
	else {
		std::cout << "No path found" << std::endl;
	}
	return ans;
}

inline bool operator<(const frontierVar & mine, const frontierVar & other)
{
	return mine.val<other.val;
}
