#include "GOAP_Flee.h"

void GOAP_Flee::enter(Agent * self, Agent * enemy)
{
	actionCost = 8;
	name = "Flee";
	taskIsDone = false;
}

void GOAP_Flee::update(Agent * self, Agent * enemy, float dtime)
{
	Vector2D steeringForce = self->Behavior()->Flee(self, enemy, dtime);
	self->update(steeringForce, dtime);
	if (Vector2D::Distance(self->getPosition(), enemy->getPosition()) > acceptableDist)
		taskIsDone = true;
}

void GOAP_Flee::exit(Agent * self, Agent * enemy)
{
}

void GOAP_Flee::resetState()
{
	taskIsDone = false;
}

void GOAP_Flee::effect(customBool & agentAlive, customBool & agentHasWeapon, customBool & weaponIsLoaded, customBool & agentHasBombs, customBool & enemyIsVisible, customBool & enemyIsAligned, customBool & enemyIsNear, customBool & enemyIsAlive)
{
	agentAlive = customBool::_true;
	agentHasWeapon = customBool::_false;
	weaponIsLoaded = weaponIsLoaded;
	agentHasBombs = agentHasBombs;
	enemyIsVisible = enemyIsVisible;
	enemyIsAligned = enemyIsAligned;
	enemyIsNear = enemyIsNear;
	enemyIsAlive = enemyIsAlive;
}

void GOAP_Flee::precondition(customBool & agentAlive, customBool & agentHasWeapon, customBool & weaponIsLoaded, customBool & agentHasBombs, customBool & enemyIsVisible, customBool & enemyIsAligned, customBool & enemyIsNear, customBool & enemyIsAlive)
{
	agentAlive = customBool::_true;
	agentHasWeapon = customBool::_false;
	weaponIsLoaded = customBool::_irrelevant;
	agentHasBombs = customBool::_irrelevant;
	enemyIsVisible = customBool::_irrelevant;
	enemyIsAligned = customBool::_irrelevant;
	enemyIsNear = customBool::_irrelevant;
	enemyIsAlive = customBool::_irrelevant;
}

void GOAP_Flee::effectHypotesis(bool & agentAlive, bool & agentHasWeapon, bool & weaponIsLoaded, bool & agentHasBombs, bool & enemyIsVisible, bool & enemyIsAligned, bool & enemyIsNear, bool & enemyIsAlive)
{
}

