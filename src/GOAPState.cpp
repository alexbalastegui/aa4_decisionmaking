#include "GOAPState.h"


bool equal(customBool a, customBool b) {
	return a == b || b == customBool::_irrelevant;	
}

void GOAPState::printName()
{
	std::cout << name.c_str() << std::endl;
}

void GOAPState::addConexionIn(GOAPState * other)
{
	in.push_back(other);
	other->out.push_back(this);
}

void GOAPState::addConexionOut(GOAPState * other)
{
	out.push_back(other);
	other->in.push_back(this);
}

bool GOAPState::TaskDone()
{
	return taskIsDone;
}

inline bool operator<(const GOAPState & mine, const GOAPState & other)
{
	return mine.actionCost < other.actionCost;
}


