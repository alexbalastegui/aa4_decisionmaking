#include "FSMState.h"

FSMState* FSMState::me = nullptr;

FSMState::FSMState() {
	if (me=nullptr) 
		throw std::logic_error("Instance already exists");
	me = this;
}

void FSMState::InitInstance()
{
	new FSMState;
}

FSMState *& FSMState::Instance()
{
	if (me == nullptr)
		me = new FSMState();
	return me;
}
