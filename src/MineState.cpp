#include "MineState.h"
#include <iostream>


void MineState::InitInstance()
{
	new MineState;
}

void MineState::Enter(Agent *a)
{

}

void MineState::Update(float dtime, Agent *a)
{
	localThirst = a->getThirst();
	localWealth = a->getWealth();

	localThirst += dtime * (rand() % 30);
	localWealth += dtime * (rand() % 10);

	a->setThirst(localThirst);
	a->setWealth(localWealth);

}

void MineState::Exit(Agent *a)
{
}
