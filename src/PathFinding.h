#pragma once
#include <vector>
#include <map>
#include <queue>
#include "Agent.h"
#include "Vector2D.h"
#include "Path.h"
#include "GOAPState.h"
#include "GOAP_Explore.h"
#include "GOAP_GetCloser.h"
#include "GOAP_Reload.h"
#include "GOAP_Aim.h"
#include "GOAP_Shoot.h"
#include "GOAP_DetonateBomb.h"
#include "GOAP_Flee.h"


class Agent;

struct frontierVar {
	GOAPState* node;
	int val;
};

inline bool operator<(const frontierVar &mine, const frontierVar  &other);

class PathFinding
{
public:
	PathFinding();
	~PathFinding();

	std::vector<GOAPState*> PathFinding::AStar(GOAPState* source, GOAPState* target);
};