#pragma once
#include"GOAPState.h"
#include "utils.h"
#include "Agent.h"

class GOAP_Aim : public GOAPState {
private:
	Vector2D lookAtVec;

public:

	std::string name = "aim";

	float acceptableRad = 10;

	void enter(Agent* self, Agent* enemy) override;
	void update(Agent* self, Agent* enemy, float dtime) override;
	void exit(Agent* self, Agent* enemy) override;
	void resetState() override;
	
	void effect(customBool &agentAlive,
		customBool &agentHasWeapon,
		customBool &weaponIsLoaded,
		customBool &agentHasBombs,
		customBool &enemyIsVisible,
		customBool &enemyIsAligned,
		customBool &enemyIsNear,
		customBool &enemyIsAlive) override;

	void precondition(customBool &agentAlive,
		customBool &agentHasWeapon,
		customBool &weaponIsLoaded,
		customBool &agentHasBombs,
		customBool &enemyIsVisible,
		customBool &enemyIsAligned,
		customBool &enemyIsNear,
		customBool &enemyIsAlive) override;

	void effectHypotesis(bool &agentAlive,
						bool &agentHasWeapon,
						bool &weaponIsLoaded,
						bool &agentHasBombs,
						bool &enemyIsVisible,
						bool &enemyIsAligned,
						bool &enemyIsNear,
						bool &enemyIsAlive) override;
};
