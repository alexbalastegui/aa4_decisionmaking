#include "GOAP_Reload.h"


void GOAP_Reload::enter(Agent * self, Agent * enemy)
{
	actionCost = 6;
	name = "Reload";
	taskIsDone = false;
}

void GOAP_Reload::update(Agent * self, Agent * enemy, float dtime)
{
	timeToReload -= dtime;

	if (timeToReload <= 0.f) {
		taskIsDone = true;
	}
}

void GOAP_Reload::exit(Agent * self, Agent * enemy)
{
}

void GOAP_Reload::resetState()
{
	taskIsDone = false;
	timeToReload = 3.5f;
}

void GOAP_Reload::effect(customBool & agentAlive, customBool & agentHasWeapon, customBool & weaponIsLoaded, customBool & agentHasBombs, customBool & enemyIsVisible, customBool & enemyIsAligned, customBool & enemyIsNear, customBool & enemyIsAlive)
{
	agentAlive = customBool::_true;
	agentHasWeapon = customBool::_true;
	weaponIsLoaded = customBool::_true;
	agentHasBombs = agentHasBombs;
	enemyIsVisible = customBool::_true;
	enemyIsAligned = enemyIsAligned;
	enemyIsNear = customBool::_true;
	enemyIsAlive = customBool::_true;
}

void GOAP_Reload::precondition(customBool & agentAlive, customBool & agentHasWeapon, customBool & weaponIsLoaded, customBool & agentHasBombs, customBool & enemyIsVisible, customBool & enemyIsAligned, customBool & enemyIsNear, customBool & enemyIsAlive)
{
	agentAlive = customBool::_true;
	agentHasWeapon = customBool::_true;
	weaponIsLoaded = customBool::_false;
	agentHasBombs = customBool::_irrelevant;
	enemyIsVisible = customBool::_true;
	enemyIsAligned = customBool::_irrelevant;
	enemyIsNear = customBool::_true;
	enemyIsAlive = customBool::_true;
}

void GOAP_Reload::effectHypotesis(bool & agentAlive, bool & agentHasWeapon, bool & weaponIsLoaded, bool & agentHasBombs, bool & enemyIsVisible, bool & enemyIsAligned, bool & enemyIsNear, bool & enemyIsAlive)
{
	weaponIsLoaded = true;
}
