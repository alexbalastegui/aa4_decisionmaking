#include "GOAP_GetCloser.h"

void GOAP_GetCloser::enter(Agent * self, Agent * enemy)
{
	actionCost = 2;
	name = "GetCloser";
	taskIsDone = false;
}

void GOAP_GetCloser::update(Agent * self, Agent * enemy, float dtime)
{
	Vector2D steeringForce = self->Behavior()->Arrive(self, enemy, 100, dtime);
	self->update(steeringForce, dtime);
	if (Vector2D::Distance(self->getPosition(), enemy->getPosition()) < acceptableDist)
		taskIsDone = true;
}

void GOAP_GetCloser::exit(Agent * self, Agent * enemy)
{
}

void GOAP_GetCloser::resetState()
{
	taskIsDone = false;
}

void GOAP_GetCloser::effect(customBool & agentAlive, customBool & agentHasWeapon, customBool & weaponIsLoaded, customBool & agentHasBombs, customBool & enemyIsVisible, customBool & enemyIsAligned, customBool & enemyIsNear, customBool & enemyIsAlive)
{
	agentAlive = customBool::_true;
	agentHasWeapon = customBool::_true;
	weaponIsLoaded = weaponIsLoaded;
	agentHasBombs = agentHasBombs;
	enemyIsVisible = customBool::_true;
	enemyIsAligned = enemyIsAligned;
	enemyIsNear = customBool::_true;
	enemyIsAlive = customBool::_true;
}

void GOAP_GetCloser::precondition(customBool & agentAlive, customBool & agentHasWeapon, customBool & weaponIsLoaded, customBool & agentHasBombs, customBool & enemyIsVisible, customBool & enemyIsAligned, customBool & enemyIsNear, customBool & enemyIsAlive)
{
	agentAlive = customBool::_true;
	agentHasWeapon = customBool::_true;
	weaponIsLoaded = customBool::_irrelevant;
	agentHasBombs = customBool::_irrelevant;
	enemyIsVisible = customBool::_true;
	enemyIsAligned = customBool::_irrelevant;
	enemyIsNear = customBool::_false;
	enemyIsAlive = customBool::_true;
}

void GOAP_GetCloser::effectHypotesis(bool & agentAlive, bool & agentHasWeapon, bool & weaponIsLoaded, bool & agentHasBombs, bool & enemyIsVisible, bool & enemyIsAligned, bool & enemyIsNear, bool & enemyIsAlive)
{
	enemyIsNear = true;
}
