#include "GOAP_Explore.h"

void GOAP_Explore::enter(Agent * self, Agent * enemy)
{
	actionCost = 4;
	name = "Explore";
	taskIsDone = false;
}

void GOAP_Explore::update(Agent * self, Agent * enemy, float dtime)
{

	Vector2D point = enemy->getPosition();
	Vector2D apex = self->getPosition();
	Vector2D base = apex + Vector2D::Normalize(self->getVelocity()) * 500;
	float angle = 30;
	
	if (Vector2DUtils::IsInsideCone(point, apex, base, angle)) {
		taskIsDone = true;
	}

	draw_circle(TheApp::Instance()->getRenderer(), (int)base.x, (int)base.y, 15, 255, 255, 255, 255);

	Vector2D force = self->Behavior()->OwnWander(self, 350, 200, 10, 2, dtime);
	self->update(force, dtime);
}

void GOAP_Explore::exit(Agent * self, Agent * enemy)
{
}

void GOAP_Explore::resetState()
{
	taskIsDone = false;
}

void GOAP_Explore::effect(customBool & agentAlive, customBool & agentHasWeapon, customBool & weaponIsLoaded, customBool & agentHasBombs, customBool & enemyIsVisible, customBool & enemyIsAligned, customBool & enemyIsNear, customBool & enemyIsAlive)
{
	agentAlive = customBool::_true;
	agentHasWeapon = customBool::_true;
	weaponIsLoaded = weaponIsLoaded;
	agentHasBombs = agentHasBombs;
	enemyIsVisible = customBool::_true;
	enemyIsAligned = customBool::_true;
	enemyIsNear = customBool::_false;
	enemyIsAlive = customBool::_true;
}

void GOAP_Explore::precondition(customBool & agentAlive, customBool & agentHasWeapon, customBool & weaponIsLoaded, customBool & agentHasBombs, customBool & enemyIsVisible, customBool & enemyIsAligned, customBool & enemyIsNear, customBool & enemyIsAlive)
{
	agentAlive = customBool::_true;
	agentHasWeapon = customBool::_true;
	weaponIsLoaded = customBool::_irrelevant;
	agentHasBombs = customBool::_irrelevant;
	enemyIsVisible = customBool::_false;
	enemyIsAligned = customBool::_irrelevant;
	enemyIsNear = customBool::_false;
	enemyIsAlive = customBool::_true;
}

void GOAP_Explore::effectHypotesis(bool & agentAlive, bool & agentHasWeapon, bool & weaponIsLoaded, bool & agentHasBombs, bool & enemyIsVisible, bool & enemyIsAligned, bool & enemyIsNear, bool & enemyIsAlive)
{
	enemyIsVisible = true;
}

