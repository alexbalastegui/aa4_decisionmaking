#pragma once
#include <vector>
#include <fstream>
#include <sstream>
#include <string>
#include <time.h>
#include "Scene.h"
#include "Agent.h"
#include "GOAPState.h"
#include "GOAP_Explore.h"
#include "GOAP_GetCloser.h"
#include "GOAP_Reload.h"
#include "GOAP_Aim.h"
#include "GOAP_Shoot.h"
#include "GOAP_DetonateBomb.h"
#include "GOAP_Flee.h"

class SceneTest :
	public Scene
{
public:
	SceneTest();
	~SceneTest();
	void update(float dtime, SDL_Event *event);
	void draw();
	const char* getTitle();
private:
	std::vector<Agent*> agents;
	Agent* enemy;
	Vector2D coinPosition;

	GOAPState* explore;
	GOAPState* getCloser;
	GOAPState* loadWeapon;
	GOAPState* aim;
	GOAPState* shoot;
	GOAPState* detonateBomb;
	GOAPState* flee;

	int num_cell_x;
	int num_cell_y;
	bool draw_grid;

	void initMaze(char* filename);
	std::vector< std::vector<int> > terrain;

	Vector2D cell2pix(Vector2D cell);
	Vector2D pix2cell(Vector2D pix);
	bool isValidCell(Vector2D cell);

	void drawMaze();
	void drawCoin();
	SDL_Texture *background_texture;
	SDL_Texture *coin_texture;
	bool loadTextures(char* filename_bg, char* filename_coin);

	std::vector<GOAPState*> actionRoute;
	int currentAction;

	GOAPState* getRandomState();
	
};
