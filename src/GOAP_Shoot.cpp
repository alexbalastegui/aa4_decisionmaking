#include "GOAP_Shoot.h"

bool GOAP_Shoot::ShotLanded()
{
	actionCost = 7;
	return timeToShoot<0.f?landShot:false;
}

void GOAP_Shoot::enter(Agent * self, Agent * enemy)
{
	//landShot = rand() % 2;
	name = "Shoot";
	taskIsDone = false;
}

void GOAP_Shoot::update(Agent * self, Agent * enemy, float dtime)
{
	timeToShoot -= dtime;

	if (timeToShoot <= 0.f) {
		taskIsDone = true;
	}
}

void GOAP_Shoot::exit(Agent * self, Agent * enemy)
{
}

void GOAP_Shoot::resetState()
{
	taskIsDone = false;
	timeToShoot = 1.5f;
}

void GOAP_Shoot::effect(customBool & agentAlive, customBool & agentHasWeapon, customBool & weaponIsLoaded, customBool & agentHasBombs, customBool & enemyIsVisible, customBool & enemyIsAligned, customBool & enemyIsNear, customBool & enemyIsAlive)
{
	agentAlive = customBool::_true;
	agentHasWeapon = customBool::_true;
	weaponIsLoaded = customBool::_true;
	agentHasBombs = agentHasBombs;
	enemyIsVisible = customBool::_true;
	enemyIsAligned = customBool::_true;
	enemyIsNear = customBool::_true;
	enemyIsAlive = customBool::_false;
}

void GOAP_Shoot::precondition(customBool & agentAlive, customBool & agentHasWeapon, customBool & weaponIsLoaded, customBool & agentHasBombs, customBool & enemyIsVisible, customBool & enemyIsAligned, customBool & enemyIsNear, customBool & enemyIsAlive)
{
	agentAlive = customBool::_true;
	agentHasWeapon = customBool::_true;
	weaponIsLoaded = customBool::_true;
	agentHasBombs = customBool::_irrelevant;
	enemyIsVisible = customBool::_true;
	enemyIsAligned = customBool::_true;
	enemyIsNear = customBool::_true;
	enemyIsAlive = customBool::_true;
}

void GOAP_Shoot::effectHypotesis(bool & agentAlive, bool & agentHasWeapon, bool & weaponIsLoaded, bool & agentHasBombs, bool & enemyIsVisible, bool & enemyIsAligned, bool & enemyIsNear, bool & enemyIsAlive)
{
	weaponIsLoaded = false;
	enemyIsAlive = false;
}

