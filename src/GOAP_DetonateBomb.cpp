#include "GOAP_DetonateBomb.h"

bool GOAP_DetonateBomb::ExplosiveLanded()
{
	return timeToDetonate<0.f ? landExplosive : false;
}

void GOAP_DetonateBomb::enter(Agent * self, Agent * enemy)
{
	actionCost = 2;
	landExplosive = rand() % 2;
	name = "DetonateBomb";
	taskIsDone = false;
}

void GOAP_DetonateBomb::update(Agent * self, Agent * enemy, float dtime)
{
	timeToDetonate -= dtime;

	if (timeToDetonate <= 0.f) {
		taskIsDone = true;
	}
}

void GOAP_DetonateBomb::exit(Agent * self, Agent * enemy)
{
}

void GOAP_DetonateBomb::resetState()
{
	taskIsDone = false;
	timeToDetonate = 1.5f;
}

void GOAP_DetonateBomb::effect(customBool & agentAlive, customBool & agentHasWeapon, customBool & weaponIsLoaded, customBool & agentHasBombs, customBool & enemyIsVisible, customBool & enemyIsAligned, customBool & enemyIsNear, customBool & enemyIsAlive)
{
	agentAlive = customBool::_true;
	agentHasWeapon = customBool::_true;
	weaponIsLoaded = weaponIsLoaded;
	agentHasBombs = customBool::_false;
	enemyIsVisible = customBool::_true;
	enemyIsAligned = enemyIsAligned;
	enemyIsNear = customBool::_true;
	enemyIsAlive = customBool::_false;
}

void GOAP_DetonateBomb::precondition(customBool & agentAlive, customBool & agentHasWeapon, customBool & weaponIsLoaded, customBool & agentHasBombs, customBool & enemyIsVisible, customBool & enemyIsAligned, customBool & enemyIsNear, customBool & enemyIsAlive)
{
	agentAlive = customBool::_true;
	agentHasWeapon = customBool::_true;
	weaponIsLoaded = customBool::_irrelevant;
	agentHasBombs = customBool::_true;
	enemyIsVisible = customBool::_true;
	enemyIsAligned = customBool::_irrelevant;
	enemyIsNear = customBool::_true;
	enemyIsAlive = customBool::_true;
}

void GOAP_DetonateBomb::effectHypotesis(bool & agentAlive, bool & agentHasWeapon, bool & weaponIsLoaded, bool & agentHasBombs, bool & enemyIsVisible, bool & enemyIsAligned, bool & enemyIsNear, bool & enemyIsAlive)
{
	enemyIsAlive = false;
	agentHasBombs = false;
}
