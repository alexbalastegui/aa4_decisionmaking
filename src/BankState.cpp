#include "BankState.h"
#include <iostream>


void BankState::InitInstance()
{
	new BankState;
}

void BankState::Enter(Agent *a){}

void BankState::Update(float dtime, Agent *a)
{
	localWealth = a->getWealth();
	localBank = a->getBank();

	float aux= dtime * (rand() % 10);

	localWealth -= aux;
	localBank += aux;

	a->setWealth(localWealth);
	a->setBank(localBank);

}

void BankState::Exit(Agent *a) {}