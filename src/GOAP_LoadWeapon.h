#pragma once
#include"GOAPState.h"
#include "utils.h"

class GOAP_LoadWeapon : public GOAPState {
private:

public:
	float timeToLoad = 1.0f;
	bool ExplosiveLanded();

	void enter(Agent* self, Agent* enemy) override;
	void update(Agent* self, Agent* enemy, float dtime) override;
	void exit(Agent* self, Agent* enemy) override;
};
