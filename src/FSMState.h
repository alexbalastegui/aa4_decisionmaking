#pragma once
class Agent;
#include <iostream>

enum class State
{
	_Mine,
	_Saloon,
	_Bank,
	_Max
};  

class FSMState
{
private:
	static FSMState* me;

public:
	virtual void Enter(Agent*a) {};
	virtual void Update(float dtime, Agent*a) {};
	virtual void Exit(Agent*a) {};

	static void InitInstance();
	static FSMState*& Instance();

protected:
	FSMState();

};
