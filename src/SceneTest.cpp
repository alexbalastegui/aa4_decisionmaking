#include "SceneTest.h"
#include "MineState.h"
#include "SaloonState.h"
#include "BankState.h"

using namespace std;

SceneTest::SceneTest()
{

	system("cls");
	draw_grid = false;

	num_cell_x = SRC_WIDTH / CELL_SIZE;
	num_cell_y = SRC_HEIGHT / CELL_SIZE;
	initMaze("../res/maze.csv");
	loadTextures("../res/maze.png", "../res/coin.png");

	srand((unsigned int)time(NULL));

	Agent *agent = new Agent;

	agent->loadSpriteTexture("../res/soldier.png", 4);
	agent->setTarget(Vector2D(-20, -20));
	agents.push_back(agent);

	enemy = new Agent;
	enemy->loadSpriteTexture("../res/soldier.png", 4);

	// set agent position coords to the center of a random cell
	Vector2D rand_cell(-1, -1);
	while (!isValidCell(rand_cell))
		rand_cell = Vector2D((float)(rand() % num_cell_x), (float)(rand() % num_cell_y));
	agents[0]->setPosition(cell2pix(rand_cell));

	rand_cell = { -1, -1 };
	while (!isValidCell(rand_cell))
		rand_cell = Vector2D((float)(rand() % num_cell_x), (float)(rand() % num_cell_y));
	enemy->setPosition(cell2pix(rand_cell));

	// set the coin in a random cell (but at least 3 cells far from the agent)
	coinPosition = Vector2D(-1, -1);
	while ((!isValidCell(coinPosition)) || (Vector2D::Distance(coinPosition, rand_cell)<3))
		coinPosition = Vector2D((float)(rand() % num_cell_x), (float)(rand() % num_cell_y));

	agents[0]->ChangeState(MineState::Instance());
	agents[0]->fsmUpdate = 0;
	agents[0]->setVelocity({ 1, 0 });
	enemy->ChangeState(MineState::Instance());
	
	explore = new GOAP_Explore();
	getCloser = new GOAP_GetCloser();
	loadWeapon = new GOAP_Reload();
	aim = new GOAP_Aim();
	shoot = new GOAP_Shoot();
	detonateBomb = new GOAP_DetonateBomb();
	flee = new GOAP_Flee();




	explore->addConexionOut(getCloser);
	getCloser->addConexionOut(loadWeapon);
	getCloser->addConexionOut(aim);
	getCloser->addConexionOut(detonateBomb);
	loadWeapon->addConexionOut(aim);
	aim->addConexionOut(shoot);
	shoot->addConexionOut(getCloser);
	shoot->addConexionOut(loadWeapon);
	shoot->addConexionOut(aim);
	detonateBomb->addConexionOut(getCloser);

	getCloser->addConexionOut(explore);
	loadWeapon->addConexionOut(explore);
	aim->addConexionOut(explore);
	shoot->addConexionOut(explore);
	detonateBomb->addConexionOut(explore);

	explore->addConexionOut(flee);
	getCloser->addConexionOut(flee);
	loadWeapon->addConexionOut(flee);
	aim->addConexionOut(flee);
	shoot->addConexionOut(flee);
	detonateBomb->addConexionOut(flee);
		

	explore->enter(agents[0], enemy);
	getCloser->enter(agents[0], enemy);
	loadWeapon->enter(agents[0], enemy);
	aim->enter(agents[0], enemy);
	shoot->enter(agents[0], enemy);
	detonateBomb->enter(agents[0], enemy);
	flee->enter(agents[0], enemy);



	actionRoute = agents[0]->RouteSearch()->AStar(getRandomState(), getRandomState());
	currentAction = actionRoute.size()-1;
}

SceneTest::~SceneTest()
{
	if (background_texture)
		SDL_DestroyTexture(background_texture);
	if (coin_texture)
		SDL_DestroyTexture(coin_texture);

	for (int i = 0; i < (int)agents.size(); i++)
	{
		delete agents[i];
	}
	delete enemy;
}



void SceneTest::update(float dtime, SDL_Event *event)
{
	/* Keyboard & Mouse events */
	switch (event->type) {
	case SDL_KEYDOWN:
		if (event->key.keysym.scancode == SDL_SCANCODE_SPACE)
			draw_grid = !draw_grid;
		break;
	case SDL_MOUSEMOTION:
	case SDL_MOUSEBUTTONDOWN:
		if (event->button.button == SDL_BUTTON_LEFT)
		{
			enemy->setPosition({ static_cast<float>(event->button.x), static_cast<float>(event->button.y)});
		}
		break;
	default:
		break;
	}

	//Vector2D steering_force = agents[0]->Behavior()->OwnWander(agents[0], 200, 100, 10, 1, dtime);
	//agents[0]->update(steering_force, dtime, event);

	if (currentAction >= 0) {
		if (!actionRoute.at(currentAction)->TaskDone())
			actionRoute.at(currentAction)->update(agents[0], enemy, dtime);
		else {
			std::cout << "\t" << "Task " << typeid(*actionRoute.at(currentAction)).name() << " completed!" << std::endl;

			currentAction--;
		}
	}
	else {
		std::cout << "all routes completed" << std::endl;

		explore->resetState();
		getCloser->resetState();
		loadWeapon->resetState();
		aim->resetState();
		shoot->resetState();
		detonateBomb->resetState();
		flee->resetState();

		actionRoute=agents.at(0)->RouteSearch()->AStar(getRandomState(), getRandomState());
		currentAction = actionRoute.size() - 1;
		std::cout << std::endl;
	}



	
}

void SceneTest::draw()
{
	drawMaze();
	drawCoin();

	if (draw_grid)
	{
		SDL_SetRenderDrawColor(TheApp::Instance()->getRenderer(), 255, 255, 255, 127);
		for (int i = 0; i < SRC_WIDTH; i += CELL_SIZE)
		{
			SDL_RenderDrawLine(TheApp::Instance()->getRenderer(), i, 0, i, SRC_HEIGHT);
		}
		for (int j = 0; j < SRC_HEIGHT; j = j += CELL_SIZE)
		{
			SDL_RenderDrawLine(TheApp::Instance()->getRenderer(), 0, j, SRC_WIDTH, j);
		}
	}

	agents[0]->draw();
	enemy->draw();


}

const char* SceneTest::getTitle()
{
	return "SDL Path Finding :: GOAP Scene";
}

void SceneTest::drawMaze()
{
	SDL_SetRenderDrawColor(TheApp::Instance()->getRenderer(), 0, 0, 255, 255);
	SDL_Rect rect;
	Vector2D coords;
	for (int j = 0; j < num_cell_y; j++)
	{
		for (int i = 0; i < num_cell_x; i++)
		{
			switch (terrain[j][i])
			{
			case 0:
				SDL_SetRenderDrawColor(TheApp::Instance()->getRenderer(), 0, 0, 255, 255);
				break;
			case 1: // Do not draw if it is not necessary (bg is already black)
			default:
				continue;
			}

			coords = cell2pix(Vector2D(i, j)) - Vector2D((float)CELL_SIZE / 2, (float)CELL_SIZE / 2);
			rect = { (int)coords.x, (int)coords.y, CELL_SIZE, CELL_SIZE };
			SDL_RenderFillRect(TheApp::Instance()->getRenderer(), &rect);
			//Alternative: render a tile texture:
			//SDL_RenderCopyEx(TheApp::Instance()->getRenderer(), tile_textures[0], .... );
		}
	}
	//Alternative: render a backgroud texture:
	//SDL_RenderCopy(TheApp::Instance()->getRenderer(), background_texture, NULL, NULL );
}

void SceneTest::drawCoin()
{
	Vector2D coin_coords = cell2pix(coinPosition);
	int offset = CELL_SIZE / 2;
	SDL_Rect dstrect = { (int)coin_coords.x - offset, (int)coin_coords.y - offset, CELL_SIZE, CELL_SIZE };
	SDL_RenderCopy(TheApp::Instance()->getRenderer(), coin_texture, NULL, &dstrect);
}

void SceneTest::initMaze(char* filename)
{
	// Initialize the terrain matrix from file (for each cell a zero value indicates it's a wall, positive values indicate terrain cell cost)
	std::ifstream infile(filename);
	std::string line;
	while (std::getline(infile, line))
	{
		vector<int> terrain_row;
		std::stringstream lineStream(line);
		std::string cell;
		while (std::getline(lineStream, cell, ','))
			terrain_row.push_back(atoi(cell.c_str()));
		SDL_assert(terrain_row.size() == num_cell_x);
		terrain.push_back(terrain_row);
	}
	SDL_assert(terrain.size() == num_cell_y);
	infile.close();
}

bool SceneTest::loadTextures(char* filename_bg, char* filename_coin)
{
	SDL_Surface *image = IMG_Load(filename_bg);
	if (!image) {
		cout << "IMG_Load: " << IMG_GetError() << endl;
		return false;
	}
	background_texture = SDL_CreateTextureFromSurface(TheApp::Instance()->getRenderer(), image);

	if (image)
		SDL_FreeSurface(image);

	image = IMG_Load(filename_coin);
	if (!image) {
		cout << "IMG_Load: " << IMG_GetError() << endl;
		return false;
	}
	coin_texture = SDL_CreateTextureFromSurface(TheApp::Instance()->getRenderer(), image);

	if (image)
		SDL_FreeSurface(image);

	return true;
}

GOAPState * SceneTest::getRandomState()
{
	int index = rand() % 7;

	switch (index) {
	case 0: return flee;
		break;
	case 1: return explore;
		break;
	case 2: return getCloser;
		break;
	case 3: return loadWeapon;
		break;
	case 4: return aim;
		break;
	case 5: return shoot;
		break;
	case 6: return detonateBomb;
		break;
	}
}

Vector2D SceneTest::cell2pix(Vector2D cell)
{
	int offset = CELL_SIZE / 2;
	return Vector2D(cell.x*CELL_SIZE + offset, cell.y*CELL_SIZE + offset);
}

Vector2D SceneTest::pix2cell(Vector2D pix)
{
	return Vector2D((float)((int)pix.x / CELL_SIZE), (float)((int)pix.y / CELL_SIZE));
}

bool SceneTest::isValidCell(Vector2D cell)
{
	if ((cell.x < 0) || (cell.y < 0) || (cell.y >= terrain.size()) || (cell.x >= terrain[0].size()))
		return false;
	return !(terrain[(unsigned int)cell.y][(unsigned int)cell.x] == 0);
}