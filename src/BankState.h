#pragma once
extern class FSMState;

#include "FSMState.h"
#include "Agent.h"


class BankState : public FSMState {
public:
	static void InitInstance();

	void Enter(Agent*a);
	void Update(float dtime, Agent*a);
	void Exit(Agent*a);

	float localWealth;
	float localThirst;
	float localBank;
};
