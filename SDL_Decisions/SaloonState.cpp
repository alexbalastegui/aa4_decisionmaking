#include "SaloonState.h"
#include <iostream>


void SaloonState::InitInstance()
{
	new SaloonState;
}

void SaloonState::Enter(Agent *a)
{

}

void SaloonState::Update(float dtime, Agent *a)
{
	localThirst = a->getThirst();
	localWealth = a->getWealth();

	localThirst += dtime * (rand() % 30);
	localWealth += dtime * (rand() % 10);

	a->setThirst(localThirst);
	a->setWealth(localWealth);

}

void SaloonState::Exit(Agent *a)
{
}
